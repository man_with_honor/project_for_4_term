#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <string>
#include "cppexp.h"
#include <QDebug>
#include <fstream>

const long double pi = 3.141592653589793238462643383279502884;

class Series {
public:
    long double a0;
    QVector <long double> a, b;
    int N;
};

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

long double integral(std::string func, long double x1, long double x2) {
    qDebug() << QString::fromStdString(func);
   /* CppExpMachine m;
    m.loadExpr(func);
    int k;
    long double h = (x2 - x1) / 10000;
    long double res = 0;
    long double x;
    long double t;
    ofstream f1("iii");

    for(k = 0; k < 10000; k++) {
        x = x1 + (k + 1/2)* ((x2 - x1) / 10000);

        m.loadVars({{"x", x}});
        t = (m.compute());
       // f1 << x << " " << t << '\n';
        res += t;
    }
    f1.close();
    res *= h;
    return res;*/
    const int N = 10000;
    long double res = 0;
    long double h = (x2 - x1) / N;
    CppExpMachine m;
    int j;
    m.loadExpr(func);

    m.loadVars({{"x", x1}});
    res += m.compute();
    m.loadVars({{"x", x2}});
    res += m.compute();

    for(j = 1; j <= (N / 2) - 1; j++) {
        m.loadVars({{"x", x1 + 2 * j * h}});
        res += 2 * m.compute();
    }

    for(j = 1; j <= (N / 2); j++) {
        m.loadVars({{"x", x1 + (2 * j - 1) * h}});
        res += 4 * m.compute();
    }

    res *= (h / 3);
    return res;
}

std::string mul_cos(std::string func, int k) {
    std::string newf = "(";
    newf += func;
    newf += ")*cos(";
    newf += std::to_string(k);
    newf += "*x)";
    return newf;
}

std::string mul_sin(std::string func, int k) {
    std::string newf = "(";
    newf += func;
    newf += ")*sin(";
    newf += std::to_string(k);
    newf += "*x)";
    return newf;
}

Series transform(std::string func, int N) {
    //qDebug() << "T";
    Series newseries;
    int i;
    newseries.N = N;
    newseries.a0 = (1 / pi) * integral(func, -1 * pi, pi);
    for(i = 1; i <= N; i++) {
        newseries.a.push_back(1/(pi) * integral(mul_cos(func, i), -1 * pi, pi));
        newseries.b.push_back(1/(pi) * integral(mul_sin(func, i), -1 * pi, pi));
    }
    //qDebug() << newseries.a0;
    //qDebug() << newseries.a;
    //qDebug() << newseries.b;
    return newseries;
}

void MainWindow::on_pushButton_clicked()
{
    Series R;
    int i, c;
    const int H_C = 10;
    std::string str1 = ui->lineEdit->text().toStdString();
    size_t pos = str1.find("x");
    CppExpMachine m;
    CppExpMachine MA[H_C];
    CppExpMachine MB[H_C];
    m.loadExpr(str1);

    std::ofstream fout;
    //fout.open("log.txt");
    //fout << str1;

    //fout << std::endl << std::endl;
    /*for(i = 0; i < str1.length();i++){
            c = str1[i];
            fout << i << " " << c << '\n';
        }
    m.outRPN(fout);
*/

    /*double computin(double x) {
        double res = R.a0 / 2;
        int j;

        for(j = 0; j < H_C; j++) {
            MA[j].loadVars({{"x", (long double)X}});
            res += MA[j].compute();
            MB[j].loadVars({{"x", (long double)X}});
            res += MB[j].compute();
        }
        return res;
    }*/

    long double a = -pi;
    long double b =  pi;
    long double h = 0.05;

    int N=(b-a)/h + 2;
    QVector<double> x(N), y(N);

  i=0;

    for (long double X=a; i < N; X+=h)
    {
        x[i] = X;

            m.loadVars({{"x", X}});
        y[i] = (double)m.compute();
        i++;
    }

    ui->widget->clearGraphs();
    ui->widget->addGraph();
    qDebug() << x;
    qDebug() << y;

    ui->widget->graph(0)->setData(x, y);

    ui->widget->xAxis->setLabel("x");
    ui->widget->yAxis->setLabel("y");
    ui->widget->xAxis->setRange(a, b);

    double minY = y[0], maxY = y[0];
    for (int i=1; i<N; i++)
    {
        if (y[i]<minY) minY = y[i];
        if (y[i]>maxY) maxY = y[i];
    }
    ui->widget->yAxis->setRange(minY, maxY);//Для оси Oy
    ui->widget->replot();






    R = transform(str1, H_C);

    for(i = 0; i < H_C; i++) {
        std::string str2 = std::to_string(R.a[i]);
        str2 = mul_cos(str2, (i + 1));
        pos = str2.find(",");
        while (pos != std::string::npos) {
            str2[pos] = '.';
            pos = str2.find(",");
        }
        MA[i].loadExpr(str2);
        //MA[i].outRPN(fout);
        //qDebug() << i;
        //qDebug() << QString::fromStdString(str2);

        str2 = std::to_string(R.b[i]);
        str2 = mul_sin(str2, (i + 1));
        pos = str2.find(",");
        while (pos != std::string::npos) {
            str2[pos] = '.';
            pos = str2.find(",");
        }
        MB[i].loadExpr(str2);
       // MB[i].outRPN(fout);
        //qDebug() << QString::fromStdString(str2);
    }
 //qDebug() << "P1";


    QVector <double> x1(N), y1(N);
    i = 0;
    int j;
    for (long double X=a; i < N; X+=h)
    {
        x1[i] = (double)X;
        long double res = R.a0 / 2;
        long double t;

       // qDebug() << "X =  " << X;
       // qDebug() << res;
        for(j = 0; j < H_C; j++) {
            MA[j].loadVars({{"x", X}});
            t = MA[j].compute();
            //qDebug() << t;
            res += t;
            //qDebug() << res;
            MB[j].loadVars({{"x", X}});
            res += MB[j].compute();
            //qDebug() << res;
        }
        y1[i] = (double)res;
        i++;

    }

    ui->widget->addGraph();
    ui->widget->graph(1)->setData(x1, y1);
    ui->widget->replot();

fout.close();
   /* QMessageBox msgBox;
    double iii = integral(str1, -pi, pi);
     msgBox.setText(QString::number(iii));
     msgBox.exec();*/
}

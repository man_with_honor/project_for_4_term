// Aleksey Maksimov, 2016
// cedefops.h - instructions realizations, easy to add in the end of file

#ifndef CPP_EXP_DEFAULT_OPS_H_INCLUDED
#define CPP_EXP_DEFAULT_OPS_H_INCLUDED 0

#include "cppexp.h"

#include <cmath>
#include <stack>

using namespace std;

long double ceResidue(long double left, long double right)
{
    return static_cast<long double>(static_cast<long long>(left) % static_cast<long long>(right));
}
long double ceAdd(long double left, long double right)
{
    return left + right;
}
long double ceSub(long double left, long double right)
{
    return left - right;
}
long double ceMul(long double left, long double right)
{
    return left*right;
}
long double ceDiv(long double left, long double right)
{
    return left/right;
}
long double cePow(long double left, long double right)
{
    return std::pow(left, right);
}

//
// ^
// */
// +-%
// ()
//

long double ceSin(std::stack<long double> *stck)
{
    long double result = std::sin(stck->top());
    stck->pop();
    return result;
}

long double ceCos(std::stack<long double> *stck)
{
    long double result = std::cos(stck->top());
    stck->pop();
    return result;
}

long double ceAbs(std::stack<long double> *stck)
{
    long double result = std::abs(stck->top());
    stck->pop();
    return result;
}

long double cePow(std::stack<long double> *stck)
{
    long double result = stck->top();
    stck->pop();
    result = std::pow(stck->top(), result);
    stck->pop();
    return result;
}


void CppExpMachine::loadDefaultOps()
{
    FuncStore.clear();
    //UnOpStore.clear();
    BinOpStore.clear();

    BinOpStore.put("%", BinOpElem(ceResidue, 3));
    BinOpStore.put("+", BinOpElem(ceAdd, 2));
    BinOpStore.put("-", BinOpElem(ceSub, 2));
    BinOpStore.put("*", BinOpElem(ceMul, 4));
    BinOpStore.put("/", BinOpElem(ceDiv, 4));
    BinOpStore.put("^", BinOpElem(cePow, 5));

    BinOpStore.put("(", BinOpElem(0, 1));
    BinOpStore.put(")", BinOpElem(0, 1));
    BinOpStore.put("|", BinOpElem(0, 1));

    // add any new functions here

    FuncStore.put("sin", ceSin);
    FuncStore.put("cos", ceCos);
    FuncStore.put("abs", ceAbs);

    FuncStore.put("pow", cePow);

    FuncStore.fit();
    //UnOpStore.fit();
    BinOpStore.fit();
}


#endif

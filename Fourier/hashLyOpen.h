// Aleksey Maksimov, 2016
// hashLyOpen.h - Ly-hash function - open-address realization

#ifndef HASH_LY_OPEN_ADDRESSING_H_INCLUDED
#define HASH_LY_OPEN_ADDRESSING_H_INCLUDED

#include <vector>

#include <new>
#include <cstring>

#include <string>
using namespace std;

namespace HashTableOpen_ns
{
    const size_t NewFreeSpace = 10;
    // ESSENTIAL FOR STRCPY, STRCAT!
    inline char* allocZeroChar()
    {
        char *toRet = new char[1];
        *toRet = '\0';
        return toRet;
    }
}

template <class T> struct HashTableElem
{
    std::string key;
    T val;
    size_t id;
    HashTableElem<T>& operator=(const HashTableElem<T> &source)
    {
        key = source.key;
        val = source.val;
        id = source.id;
        return *this;
    }
};

template <class T> class HashTableOpen		// 52 Bytes
{
    static unsigned hashFuncLy_(const char *str)
    {
        unsigned hash = 0;

        for (; *str; str++)
            hash = (hash * 1664525) + (unsigned char)(*str) + 1013904223;

        return hash;
    }
    struct Elem_
    {
        T val;
        char *str;
        Elem_() : str(HashTableOpen_ns::allocZeroChar())
        {
        }
        Elem_& operator=(const Elem_ &source)
        {
            val = source.val;
            delete[] str;
            str = new char[strlen(source.str) + 1];
            strcpy(str, source.str);
            return *this;
        }
        Elem_(const Elem_ &source) : str(HashTableOpen_ns::allocZeroChar())
        {
            *this = source;
        }
        void set(const char *key, const T &newVal)
        {
            val = newVal;
            if (str)
            {
                delete[]str;
            }
            str = new char[strlen(key) + 1];
            strcpy(str, key);
        }
        ~Elem_()
        {
            delete[] str;
        }
    };
    std::vector<Elem_>		Store_;							//16B
    std::vector<bool>		BoolStore_;						//24B
    size_t					emptySpace_;					//4B
    bool					autoAllocF_;					//1B (->4B)// Flag
    unsigned				(*hashFunc_)(const char *str);	//4B

    unsigned findValIndex_(const char *str)
    {
        if (!str)
        {
            return Store_.size();
        }
        unsigned iSave = hashFunc_(str) % Store_.size();
        unsigned i = 0;
        bool result = false;
        while (i < BoolStore_.size())
        {
            if (BoolStore_[i] && !strcmp(Store_[i].str, str))
            {
                result = true;
                break;
            }
            i++;
        }
        if (i == BoolStore_.size())
        {
            i = 0;
            while (i < iSave)
            {
                if (BoolStore_[i] && !strcmp(Store_[i].str, str))
                {
                    result = true;
                    break;
                }
                i++;
            }
        }
        if (result)
        {
            return i;
        }
        else
        {
            return Store_.size();
        }
    }
    void makeAutoLessTry_()
    {
        if (autoAllocF_ && emptySpace_ >= HashTableOpen_ns::NewFreeSpace*2)
        {
            resize(size() + HashTableOpen_ns::NewFreeSpace);
        }
    }
public:
    HashTableOpen(size_t startTableSize = HashTableOpen_ns::NewFreeSpace)
        : Store_(startTableSize), BoolStore_(startTableSize, false),
        emptySpace_(startTableSize), hashFunc_(hashFuncLy_), autoAllocF_(true)
    {
    }
    HashTableOpen<T>& operator=(HashTableOpen<T> &source)
    {
        Store_ = source.Store_;
        BoolStore_ = source.BoolStore_;
        autoAllocF_ = source.autoAllocF_;
        hashFunc_ = source.hashFunc_;
        emptySpace_ = source.emptySpace_;
        return *this;
    }
    bool put(const char *key, const T &val)
    {
        if (!key || !*key)
        {
            return false;
        }
        unsigned i = 0;
        if (!Store_.empty())
        {
            i = findValIndex_(key);
            if (i < Store_.size())
            {
                Store_[i].val = val;
                return true;
            }
        }
        if (emptySpace_ == 0)
        {
            if (autoAllocF_)
            {
                resize(Store_.size() + HashTableOpen_ns::NewFreeSpace);
            }
            else
            {
                return false;
            }
        }
        unsigned iSave = hashFunc_(key) % Store_.size();
        i = 0;
        while (i < BoolStore_.size() && BoolStore_[i])
        {
            i++;
        }
        if (i == BoolStore_.size())
        {
            i = 0;
            while (BoolStore_[i] /*&& i<iSave*/)
            {
                i++;
            }
        }
        BoolStore_[i] = true;
        emptySpace_--;
        Store_[i].set(key, val);
        return true;
    }
    bool getElemByName(const char *key, HashTableElem<T> *toSaveElem)
    {
        size_t i = findValIndex_(key);
        if (i < Store_.size())
        {
            if (toSaveElem)
            {
                toSaveElem->key.assign(Store_[i].str);
                toSaveElem->val = Store_[i].val;
                toSaveElem->id = i;
            }
            return true;
        }
        else
        {
            return false;
        }
    }
    bool getElemById(const size_t id, HashTableElem<T> *toSaveElem)
    {
        if (BoolStore_[id])
        {
            if (toSaveElem)
            {
                toSaveElem->key.assign(Store_[id].str);
                toSaveElem->val = Store_[id].val;
                toSaveElem->id = id;
            }
            return true;
        }
        else
        {
            return false;
        }
    }
    bool getValByName(const char *key, T *toSaveVal)
    {
        size_t i = findValIndex_(key);
        if (i < Store_.size())
        {
            if (toSaveVal)
            {
                *toSaveVal = Store_[i].val;
            }
            return true;
        }
        else
        {
            return false;
        }
    }
    bool getValById(const size_t id, T *toSaveVal)
    {
        if (BoolStore_[id])
        {
            if (toSaveVal)
            {
                *toSaveVal = Store_[id].val;
            }
            return true;
        }
        else
        {
            return false;
        }
    }
    bool getIdByName(const char *key, size_t *toSaveId)
    {
        size_t i = findValIndex_(key);
        if (i < Store_.size())
        {
            if (toSaveId)
            {
                *toSaveId = i;
            }
            return true;
        }
        else
        {
            return false;
        }
    }
    bool getConstNameById(const size_t id, const char **toSaveName)
    {
        if (BoolStore_[id])
        {
            if (toSaveName)
            {
                *toSaveName = Store_[id].str;
            }
            return true;
        }
        else
        {
            return false;
        }
    }
    bool delElemByName(const char *key)
    {
        size_t i = findValIndex_(key);
        if (i < Store_.size())
        {
            delete[] Store_[i].str;
            Store_[i].str = HashTableOpen_ns::allocZeroChar();
            BoolStore_[i] = false;
            emptySpace_++;
            makeAutoLessTry_();
            return true;
        }
        else
        {
            return false;
        }
    }
    bool delElemById(const size_t id)
    {
        if (BoolStore_[id])
        {
            delete[] Store_[id].str;
            Store_[id].str = HashTableOpen_ns::allocZeroChar();
            BoolStore_[id] = false;
            emptySpace_++;
            makeAutoLessTry_();
            return true;
        }
        else
        {
            return false;
        }
    }
    void clear()
    {
        if (emptySpace_ != Store_.size())
        {
            size_t i = 0;
            for (i; i < BoolStore_.size(); i++)
            {
                if (BoolStore_[i])
                {
                    delete[] Store_[i].str;
                    Store_[i].str = HashTableOpen_ns::allocZeroChar();
                    BoolStore_[i] = false;
                }
            }
            emptySpace_ = Store_.size();
            makeAutoLessTry_();
        }
    }
    void resize(size_t newSize)
    {
        if (newSize >= Store_.size() - emptySpace_)
        {
            HashTableOpen<T> next(newSize);
            next.autoAllocF_ = autoAllocF_;
            auto it = Store_.cbegin();
            while (it != Store_.cend())
            {
                next.put(it->str, it->val);
                it++;
            }
            *this = next;
        }
    }
    size_t size()
    {
        return Store_.size() - emptySpace_;
    }
    size_t getAllocNum()
    {
        return Store_.size();
    }
    size_t getEmptyNum()
    {
        return emptySpace_;
    }
    bool isFit()
    {
        return getEmptyNum() == 0;
    }
    void fit()
    {
        resize(size());
    }
    void setHashFunc(unsigned(*newHashFunc)(const char *str))
    {
        hashFunc_ = newHashFunc;
    }
    void setHashFuncLy()
    {
        hashFunc_ = hashFuncLy_;
    }
};


#endif

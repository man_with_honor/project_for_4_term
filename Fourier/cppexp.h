// Aleksey Maksimov, 2016
// cppexp.h - class definition

#ifndef CPP_EXPRESSION_H_INCLUDED
#define CPP_EXPRESSION_H_INCLUDED

#include "hashLyOpen.h"

#include <ostream>
#include <stack>

using namespace std;

struct ceVar
{
    // RPN for each expression must be computed only once
    // you use a single compilation for all variables you want to use
    // name is used to load another variable value next time
    std::string name;
    long double val;
    ceVar() : val(0)
    {}
    ceVar(std::string str, long double nval = 0) : val(nval), name(str)
    {}
    ceVar& operator=(const ceVar &source)
    {
        name = source.name;
        val = source.val;
        return *this;
    }
    ceVar(const ceVar &source)
    {
        *this = source;
    }
};


class CppExpMachine // 172B
{

    struct ceWord;
    // reverse Polish notation
    std::vector<ceWord> finalRPN;
    // variables for currently loaded expression
    HashTableOpen<long double> vars;

    // bin.op. - not a function, but behaves like it;
    // has 2 arguments on the both sizes
    struct BinOpElem
    {
        long double(*func)(long double, long double);
        unsigned char priority;
        BinOpElem() : priority(0), func(nullptr)
        {}
        BinOpElem(long double(*nfunc)(long double, long double), unsigned char npriority)
            : func(nfunc), priority(npriority)
        {}
    };
    // store operations/functions for current machine
    // add new functions using "cedefops.h"
    // TODO: load additional functions dynamically
    HashTableOpen<long double(*)(std::stack<long double>*)> FuncStore;
    // HashTableOpen<long double(*)(long double)> UnOpStore;
    static const unsigned char funcPriority;
    HashTableOpen<BinOpElem> BinOpStore;

    // delete space, replase brackets ( '{([' => '(((' )
    size_t prepareCString(char *s);
    // get next item for RPN
    ceWord getNextWord(const char * const start, const char **end);
    void loadDefaultOps();
public:
    int hx;//have x ?



    // load expression
    // using std::string allows to do it in another thread
    void loadExpr(std::string s);
    // show final Reverse Polish Notation string
    void outRPN(std::ostream &out);
    CppExpMachine()
    {
        loadDefaultOps();
    }
    CppExpMachine(std::string s)
    {
        loadDefaultOps();
        loadExpr(s);
    }
    bool loadVars(std::vector<ceVar> ceVars);
    bool loadVar(std::string name, long double val);
    long double compute();
};


struct CppExpMachine::ceWord
{
    enum Type
    {
        empty = 0, // error
        constant,
        operation,
        variable
    };

    Type type;
    union
    {
        long double d;
        struct
        {
            unsigned char priority;
            size_t id;
        } op;
        size_t varid;
    }val;
    ceWord() : type(constant)
    {
        val.d = 0;
    }
    ceWord(Type nType, long double nVal) : type(nType)
    {
        if (nType == operation)
        {
            nType = empty;
        }
        switch (type)
        {
        case constant:
            val.d = nVal;
            break;
        default:
            val.varid = static_cast<size_t>(nVal);
        }
    }
    ceWord(Type nType, unsigned opId, unsigned char opPriority) : type(nType)
    {
        val.op.id = opId, val.op.priority = opPriority;
    }
};

#endif

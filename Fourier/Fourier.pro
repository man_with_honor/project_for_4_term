#-------------------------------------------------
#
# Project created by QtCreator 2016-05-06T16:01:30
#
#-------------------------------------------------

QT       += core gui
CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = Fourier
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    qcustomplot.cpp \
    cppexp.cpp

HEADERS  += mainwindow.h \
    qcustomplot.h \
    hashLyOpen.h \
    cppexp.h \
    cedefops.h

FORMS    += mainwindow.ui

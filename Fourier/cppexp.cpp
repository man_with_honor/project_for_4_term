// Aleksey Maksimov, 2016
// cppexp.cpp - class realization

#include "cppexp.h"
#include <QDebug>
#include <sstream>
#include <string>


const unsigned char CppExpMachine::funcPriority = 0;

using namespace std;

bool CppExpMachine::loadVars(std::vector<ceVar> ceVars)
{
    if (!hx) return false;
    ////////////////

    const size_t size = ceVars.size();
    size_t i = 0;
    bool result = true;
    for (i; i < size; i++)
    {
        if (vars.getIdByName(ceVars[i].name.c_str(), nullptr))
        {
            vars.put(ceVars[i].name.c_str(), ceVars[i].val);
        }
        else
        {
            result = false;
        }
    }
    return result;
}
bool CppExpMachine::loadVar(std::string name, long double val)
{
    if (!hx) return false;
    ///////

    if (vars.getIdByName(name.c_str(), nullptr))
    {
        vars.put(name.c_str(), val);
        return true;
    }
    else
    {
        return false;
    }
}

#include "cedefops.h"

inline bool isOpenBracket(char c)
{
    return c == '(' || c == '[' || c == '{';
}
inline bool isCloceBracket(char c)
{
    return c == ')' || c == ']' || c == '}';
}

inline bool isNextArgument(char c)
{
    return c == ',' || c == ';';
}

CppExpMachine::ceWord CppExpMachine::getNextWord(const char * const start, const char **end)
{
    stringstream sss;
    string SSS;
    int i;

    if (!start || !end)
    {
        return ceWord(ceWord::empty, 0);
    }
    const size_t funcIdOffset = BinOpStore.size();
    const char *cur = start;
    bool lookOpFlag = true;
    if (*cur < '0' || '9' < *cur)
    {
        lookOpFlag = false;
    }

    long double val = 0;

    if (lookOpFlag)
    {
        while ((('0' <= (*cur)) && ((*cur) <= '9')) || ((*cur) == '.'))
        {
            //qDebug() << *cur;
            cur++;
        }
        *end = cur;

        SSS = "";
        for(i = 0; i < cur - start; i++)
            SSS.push_back(start[i]);

        sss.str().resize(0);
        //qDebug() << cur - start;


        //sscanf(tbuf, "%Lg", &val);
        //qDebug() << "DDDDDDDDDDDDD" << QString::fromStdString(SSS);
        sss << SSS;
        //std::sscanf(start, "%Lg", &val);
        sss >> val;
        //qDebug() << QString::number((double)val)


        return ceWord(ceWord::constant, val);
    }
    else
    {
        cur++;
        std::string str(start, cur);
        HashTableElem<BinOpElem> binOpElem;
        if (BinOpStore.getElemByName(str.c_str(), &binOpElem))
        {
            *end = cur;
            return ceWord(ceWord::operation, binOpElem.id, binOpElem.val.priority);
        }
        else
        {
            char binTest[2] = { *cur,0 };
            while (*cur && !isOpenBracket(*cur) && !BinOpStore.getIdByName(binTest, 0) && !isNextArgument(*cur))
            {
                cur++;
                binTest[0] = *cur;
            }
            *end = cur;
            HashTableElem<long double(*)(std::stack<long double>*)> funcElem;
            str.assign(start, cur);
            if (FuncStore.getElemByName(str.c_str(), &funcElem) && isOpenBracket(*cur))
            {
                return ceWord(ceWord::operation, funcElem.id + funcIdOffset, funcPriority);
            }
            else
            {
                if (str.compare("pi") && str.compare("PI"))
                {
                    vars.put(str.c_str(), 0);
                }
                else
                {
                    vars.put(str.c_str(), 3.141592653589793238462643383279502884L);
                }
                return ceWord(ceWord::variable, vars.size() - 1);
            }
        }
    }
}

#include <cstring>
#include <cctype>

size_t CppExpMachine::prepareCString(char *s)
{
    size_t offset = 0;
    size_t size = strlen(s);
    size_t i = 0;
    for (i; i <= size; i++)
    {
        if (s[i] < 0)
        {
            // isspace has check for char>=0
            // no one garants that there are no spaces in other charsets
            s[i - offset] = s[i];
        }
        else if (std::isspace(s[i]))
        {
            offset++;
        }
        else
        {
            if (isOpenBracket(s[i]))
            {
                s[i - offset] = '(';
            }
            else if (isCloceBracket(s[i]))
            {
                s[i - offset] = ')';
            }
            else if (isNextArgument(s[i]))
            {
                s[i - offset] = ',';
            }
            else
            {
                s[i - offset] = s[i];
            }
        }
    }
    return size - offset;
}

#include <stack>

void CppExpMachine::loadExpr(std::string s)
{
    size_t pos = s.find("x");
    if (pos == std::string::npos)
        hx = 0;
    else
        hx = 1;
    /////////////

    vars.clear();
    size_t size = s.size();
    char *baseCString = nullptr;
    baseCString = new char[size + 1];
    s.copy(baseCString, size);	// copy() ingnores size>size()
    baseCString[size] = 0;		//
    s.clear();
    size_t nSize = prepareCString(baseCString);

    if (nSize < size)
    {
        baseCString = (char*)realloc(baseCString, nSize + 1);
    }

    finalRPN.clear();
    const char *cur = baseCString;

    std::stack<ceWord> opStck;


    size_t OpenBracketId = -1;
    BinOpStore.getIdByName("(", &OpenBracketId);
    size_t CloseBracketId = -1;
    BinOpStore.getIdByName(")", &CloseBracketId);
    size_t ModuleBracketId = -1;
    BinOpStore.getIdByName("|", &ModuleBracketId);
    size_t AbsFuncId = -1;
    FuncStore.getIdByName("abs", &AbsFuncId);
    AbsFuncId += BinOpStore.size();// funcId offset

    bool waitOpenModule = true;
    bool wasOpenBracket = true;

    ceWord word;

    while (*cur)
    {
        if (!isNextArgument(*cur))
        {
            word = getNextWord(cur, &cur);
            //qDebug() << QString::number((double)word.val.d);
            if (word.type == ceWord::constant || word.type == ceWord::variable)
            {
                finalRPN.push_back(word);
                waitOpenModule = false;
                wasOpenBracket = false;
            }
            else
            {
                if (word.val.op.id == CloseBracketId
                    || waitOpenModule == false && word.val.op.id == ModuleBracketId)
                {
                    while (!opStck.empty() && opStck.top().val.op.id != OpenBracketId)
                    {
                        finalRPN.push_back(opStck.top());
                        opStck.pop();
                    }
                    opStck.pop();
                    if (!opStck.empty() && opStck.top().type == ceWord::operation && !opStck.empty() && opStck.top().val.op.priority == funcPriority)
                    {
                        finalRPN.push_back(opStck.top());
                        opStck.pop();
                    }
                }
                else
                {
                    if (word.val.op.id != OpenBracketId && word.val.op.id != ModuleBracketId
                        && !opStck.empty() && word.val.op.priority <= opStck.top().val.op.priority
                        && !word.val.op.priority == 0)
                    {
                        while (!opStck.empty() && word.val.op.priority <= opStck.top().val.op.priority)
                        {
                            finalRPN.push_back(opStck.top());
                            opStck.pop();
                        }
                    }
                    if (word.val.op.id != ModuleBracketId)
                    {
                        if (word.val.op.id == OpenBracketId)
                        {
                            wasOpenBracket = true;
                        }
                        waitOpenModule = true;
                    }
                    else if (waitOpenModule)
                    {
                        wasOpenBracket = true;
                        word.val.op.id = OpenBracketId;
                        opStck.push(ceWord(ceWord::operation, AbsFuncId, funcPriority));
                    }
                    if (word.val.op.priority == 2 && wasOpenBracket)
                    {
                        finalRPN.push_back(ceWord(ceWord::constant, 0));
                    }
                    opStck.push(word);
                }
            }
        }
        else
        {
            while (!opStck.empty() && opStck.top().val.op.id != OpenBracketId)
            {
                finalRPN.push_back(opStck.top());
                opStck.pop();
            }
            cur++;
        }
    }
    if (opStck.size())
    {
        while (!opStck.empty())
        {
            if (opStck.top().val.op.id != OpenBracketId)
            {
                finalRPN.push_back(opStck.top());
            }
            opStck.pop();
        }
    }
    vars.fit();

    //DDDDDDDDDDDDDDDDDDDDDDd

   // qDebug() << std::endl;

}

void CppExpMachine::outRPN(std::ostream &out)
{
    auto it = finalRPN.cbegin();
    HashTableElem<BinOpElem> binOpElem;
    HashTableElem<long double(*)(std::stack<long double>*)> funcElem;
    HashTableElem<long double> varElem;
    while (it != finalRPN.cend())
    {
        switch (it->type)
        {
        case ceWord::constant:
            out << it->val.d << ' ';
            break;
        case ceWord::variable:
            vars.getElemById(it->val.varid, &varElem);
            out << varElem.key << ' ';
            break;
        case ceWord::operation:
            if (it->val.op.priority != 0
                && BinOpStore.getElemById(it->val.op.id, &binOpElem))
            {
                out << binOpElem.key << ' ';
            }
            else if (FuncStore.getElemById(it->val.op.id - BinOpStore.size(), &funcElem))
            {
                out << funcElem.key << ' ';
            }
            break;
        }
        it++;
    }
    out << std::endl;
}

long double CppExpMachine::compute()
{
    std::stack<long double> stck;
    const size_t funcIdOffset = BinOpStore.size();
    auto it = finalRPN.cbegin();
    long double var = 0;
    BinOpElem binElem;
    long double(*funcPtr)(std::stack<long double>*);
    while (it != finalRPN.cend())
    {
        switch (it->type)
        {
        case ceWord::constant:
            stck.push(it->val.d);
            break;
        case ceWord::variable:
            vars.getValById(it->val.varid, &var);
            stck.push(var);
            break;
        case ceWord::operation:
            if (it->val.op.id >= funcIdOffset)
            {
                FuncStore.getValById(it->val.op.id - funcIdOffset, &funcPtr);
                stck.push(funcPtr(&stck));
            }
            else
            {
                BinOpStore.getValById(it->val.op.id, &binElem);
                var = stck.top();
                stck.pop();
                var = binElem.func(stck.top(), var);
                stck.pop();
                stck.push(var);
            }
            break;
        }
        it++;
    }
    return stck.top();
}
